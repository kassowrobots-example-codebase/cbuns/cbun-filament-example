#!/bin/bash

mkdir assemble
cd assemble
mkdir cbun_filament_example
cp ../bundle.xml ./cbun_filament_example
cp ../app/build/outputs/apk/debug/app-debug.apk ./cbun_filament_example/filament_viewer.apk
tar --exclude='*.DS_Store' -cvzf  filament_example.cbun -C ./cbun_filament_example .
cp filament_example.cbun ../
cd ..
rm -rf assemble
 
