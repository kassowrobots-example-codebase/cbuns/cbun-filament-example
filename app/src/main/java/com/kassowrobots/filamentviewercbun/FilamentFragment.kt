package com.kassowrobots.filamentviewercbun

import android.os.Bundle
import android.view.*
import com.google.android.filament.utils.KtxLoader
import com.google.android.filament.utils.ModelViewer
import com.google.android.filament.utils.Utils
import com.kassowrobots.api.app.fragment.KRFragment
import java.nio.ByteBuffer


class FilamentFragment : KRFragment() {

    // static initialization of the Filament engine
    companion object {
        init { Utils.init() }
    }

    // surface view serves the Filament engine as a rendering view
    private lateinit var surfaceView: SurfaceView

    // choreographer coordinates the timing of the rendering
    private lateinit var choreographer: Choreographer

    // model viewer is responsible for the rendering of the Filament scene
    private lateinit var modelViewer: ModelViewer

    /**
     * Called to let the fragment to instantiate its view, ie. this method is responsible for
     * generating the fragment view.
     *
     * @param inflater Allows view inflating - not used.
     * @param container Parent view container - not used.
     * @param savedInstanceState Persistent state instance - not used.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // instantiate the SurfaceView as the root view of the fragment
        surfaceView = SurfaceView(context)

        // get the choreographer instance for this thread
        choreographer = Choreographer.getInstance()

        // instantiate the Filament model viewer
        modelViewer = ModelViewer(surfaceView)

        // delegate the surface view touch handling to the model viewer
        surfaceView.setOnTouchListener(modelViewer)

        // load the model
        loadGltf("BusterDrone")

        // load the environment (skybox + lighting)
        loadEnvironment("default_env")

        return surfaceView
    }

    /**
     * Frame rendering callback to be executed periodically during the render loop. Its doFrame
     * method is responsible for the rendering of the scene and applying the animation. It can be
     * also used to altering the model frames.
     */
    private val frameCallback = object : Choreographer.FrameCallback {
        private val startTime = System.nanoTime()
        override fun doFrame(currentTime: Long) {
            // calculate the time elapsed since the instantiation of this frame callback
            val seconds = (currentTime - startTime).toDouble() / 1_000_000_000

            // post this callback to the choreographer to be invoked on the next frame
            choreographer.postFrameCallback(this)

            // OPTIONAL: apply the animation - note that the animation is part of the glTF model
            modelViewer.animator?.apply {
                if (animationCount > 0) {
                    applyAnimation(0, seconds.toFloat())
                }
                updateBoneMatrices()
            }

            // render the Filament scene
            modelViewer.render(currentTime)
        }
    }

    /**
     * Called when the fragment is visible to the user and actively running.
     */
    override fun onResume() {
        super.onResume()
        // add frame callback to the choreographer to start the rendering loop
        choreographer.postFrameCallback(frameCallback)
    }

    /**
     * Called when the fragment is no more resumed.
     */
    override fun onPause() {
        super.onPause()
        // remove frame callback from the choreographer to interrupt the rendering loop
        choreographer.removeFrameCallback(frameCallback)
    }

    /**
     * Called when the fragment is no longer in use.
     */
    override fun onDestroy() {
        super.onDestroy()
        // remove frame callback from the choreographer to interrupt the rendering loop
        choreographer.removeFrameCallback(frameCallback)
    }

    /**
     * Loads glTF model from Assets into the Filament scene.
     *
     * @param name Name of the glTF model to be loaded.
     */
    private fun loadGltf(name: String) {
        // read glTF model in form of byte buffer
        val buffer = readAsset("models/${name}.gltf")

        // load model buffer into the Filament scene
        modelViewer.loadModelGltf(buffer) { uri -> readAsset("models/$uri") }

        // set the root transformation on the model to make it fit the view
        modelViewer.transformToUnitCube()
    }

    /**
     * Loads Ktx environment (lighting + skybox) into the Filament scene.
     *
     * @param name Name of the KTX environment to be loaded.
     */
    private fun loadEnvironment(name: String) {
        val engine = modelViewer.engine
        val scene = modelViewer.scene

        // initialize the lighting from the KTX asset file
        readAsset("envs/$name/${name}_ibl.ktx").let {
            scene.indirectLight = KtxLoader.createIndirectLight(engine, it)
            scene.indirectLight!!.intensity = 30_000.0f
        }

        // create the skybox from the KTX asset file
        readAsset("envs/$name/${name}_skybox.ktx").let {
            scene.skybox = KtxLoader.createSkybox(engine, it)
        }
    }

    /**
     * Reads Assets file and returns its byte buffer.
     *
     * @param path Path to the asset to be loaded.
     */
    private fun readAsset(path: String): ByteBuffer {
        // open the asset in form of the input stream
        val input = requireContext().assets.open(path)

        // read the bytes from the asset input stream
        val bytes = ByteArray(input.available())
        input.read(bytes)

        // wrap the bytes into the byte buffer
        return ByteBuffer.wrap(bytes)
    }
}